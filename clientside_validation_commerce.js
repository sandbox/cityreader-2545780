/**
 * @file
 * Javascript to enable/disable continue button in Commerce checkout pages.
 */

(function($) {

Drupal.behaviors.clientsideValidationCommerce = {
  attach: function(context, settings) {
    $('form', context).once('checkout', function() {
      var $form = $(this);
      var $icon = $form.find('.checkout-processing');

      $form
        .bind('invalid-form', function() {
          $.event.trigger('enableContinue', [document]);
        })
        .bind('submit', function() {
          $.event.trigger('disableContinue', [document]);
        });
    })

  }
}

/**
 * Override Drupal.behaviors.commerceCheckout to make continue button work
 * with client side JavaScript validation.
 *
 * @see https://www.drupal.org/node/1775750
 *
 * @todo remove this code when it is added in commerce_checkout.js
 */
Drupal.behaviors.commerceCheckout = {
  attach: function(context, settings) {
    $('input.checkout-continue', context).once('checkout').click(function() {
      var $btn = $(this);
      var $clone = $btn.clone();

      // When the buttons to move from page to page in the checkout process are
      // clicked we disable them so they are not accidentally clicked twice.
      $clone.insertAfter(this).attr('disabled', 'disabled').next().removeClass('element-invisible');
      $btn.hide();

      $(document)
        .bind('enableContinue', function() {
          Drupal.commerceCheckout.enableContinue($clone);
        })
        .bind('disableContinue', function() {
          Drupal.commerceCheckout.disableContinue($clone);
        });
    });
  }
}

Drupal.commerceCheckout = {
  /**
   * Disable continue button to process form submit.
   */
  disableContinue: function($btn) {
    $btn.attr('disabled', 'disabled');
    $btn.siblings('.checkout-processing').removeClass('element-invisible');
  },

  /**
   * Enable continue button when client side form validation is failed.
   */
  enableContinue: function($btn) {
    $btn.removeAttr('disabled');
    $btn.siblings('.checkout-processing').addClass('element-invisible');
  }
}

})(jQuery);
