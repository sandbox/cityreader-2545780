CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Maintainers


INTRODUCTION
------------
The Clientside Validation for Commerce module only provides some UX improvement
when you use clientside_validation module in the Drupal commerce site. It is
useful to fix the UX bug when you happen to use clientside_validation module
in the checkout pages of Drupal Commerce site. It hides processing icon when
form validation in JavaScript is failed and show processing icon when that
Javascript validation is successful.


* For a full description of the module, visit the project page:
  https://www.drupal.org/sandbox/cityreader/2545780


* To submit bug reports and feature suggestions, or to track changes:
  https://www.drupal.org/project/issues/2545780


REQUIREMENTS
------------
This module requires the following modules:
 * Drupal Commerce Checkout (https://drupal.org/project/commerce)
 * Clientside Validation (https://drupal.org/project/clientside_validation)


INSTALLATION
------------
 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.


MAINTAINERS
-----------
Current maintainers:
 * Eric Chen (eric.chenchao) - https://drupal.org/user/265729

